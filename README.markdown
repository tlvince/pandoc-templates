Contains custom templates as well as defaults from the [original
repository](https://github.com/jgm/pandoc-templates).
